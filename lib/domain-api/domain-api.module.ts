import { Global, HttpModule, Module } from '@nestjs/common';
import { DomainApiService } from './services/domain-api.service';
import { RxJSRetryService } from './services/rxjs-retry.service';

@Global()
@Module({
  imports: [HttpModule],
  providers: [DomainApiService, RxJSRetryService],
  exports: [DomainApiService, RxJSRetryService],
})
export class DomainApiModule {}
