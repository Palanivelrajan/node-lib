import { HttpModule, HttpService } from '@nestjs/common';

import { Test, TestingModule } from '@nestjs/testing';
import Axios, { AxiosResponse } from 'axios';
import { DependencyUtlilizationService } from '../../healthcheck/dependency-utlilization/dependency-utlilization.service';
import { DOMAINAPI_MODULE_OPTIONS } from '../constants/domain-api.constants';
import { DomainApiService } from './domain-api.service';
import { AppLoggerService } from '../../logger';
import { Observable, of, throwError, observable } from 'rxjs';
import { ConfigService, ConfigModule } from '../../config';
import { RxJSRetryService } from './rxjs-retry.service';

describe('DomainApiService', () => {
  let domainApiService: DomainApiService;
  let dependencyUtlilizationService: DependencyUtlilizationService;
  let httpService: HttpService;
  let rxJSRetryService: RxJSRetryService;

  const thrownError = new Error('Error');
  (thrownError as any).status = 500;
  const getMockResponse = { id: 1, name: 'PAL' };
  beforeEach(async () => {
    const domainAPIOption = {
      host: 'http://localhost',
      port: 3000,
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, ConfigModule.forRoot({ folder: '/lib/config' })],
      providers: [
        DomainApiService,
        AppLoggerService,
        RxJSRetryService,
        DependencyUtlilizationService,
      ],
    }).compile();

    domainApiService = module.get<DomainApiService>(DomainApiService);
    dependencyUtlilizationService = module.get<DependencyUtlilizationService>(
      DependencyUtlilizationService,
    );
    httpService = module.get<HttpService>(HttpService);
    rxJSRetryService = module.get<RxJSRetryService>(RxJSRetryService);
    //jest.setTimeout(3000);
  });

  it('should be defined', () => {
    expect(domainApiService).toBeDefined();
  });
  it('Call onModuleInit and check the  ', () => {
    jest.spyOn(domainApiService, 'onModuleInit');
    domainApiService.onModuleInit();
    expect(domainApiService.baseURL).toBe('http://localhost:3001');
  });

  it('Get Method call success and mocked value asserted', () => {
    jest
      .spyOn(httpService, 'get')
      .mockReturnValue(Observable.create(getMockResponse));

    domainApiService.get('path').subscribe({
      next: (val: any) => {
        expect(val).toEqual(getMockResponse);
        expect(httpService.get).toBeCalledTimes(1);
      },
    });
  });
  it('Get Method call throw error with status 500 rxJSRetryService should throw this error', done => {
    jest.spyOn(rxJSRetryService, 'genericRetryStrategy');
    jest.spyOn(httpService, 'get').mockReturnValue(throwError(thrownError));

    domainApiService.get('path').subscribe({
      error: err => {
        expect(err.message).toBe('Error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
    });
  });
  it('Post Method call success and mocked value asserted', () => {
    jest
      .spyOn(httpService, 'post')
      .mockReturnValue(Observable.create(getMockResponse));

    domainApiService.post('path').subscribe({
      next: (val: any) => {
        expect(val).toEqual(getMockResponse);
        expect(httpService.post).toBeCalledTimes(1);
      },
    });
  });

  it('Post Method call throw error with status 500 rxJSRetryService should throw this error', done => {
    jest.spyOn(rxJSRetryService, 'genericRetryStrategy');
    jest.spyOn(httpService, 'post').mockReturnValue(throwError(thrownError));

    domainApiService.post('path').subscribe({
      error: err => {
        expect(err.message).toBe('Error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
    });
  });

  it('Post Method call success and mocked value asserted', () => {
    jest
      .spyOn(httpService, 'put')
      .mockReturnValue(Observable.create(getMockResponse));

    domainApiService.put('path').subscribe({
      next: (val: any) => {
        expect(val).toEqual(getMockResponse);
        expect(httpService.put).toBeCalledTimes(1);
      },
    });
  });

  it('Put Method call throw error with status 500 rxJSRetryService should throw this error', done => {
    jest.spyOn(rxJSRetryService, 'genericRetryStrategy');
    jest.spyOn(httpService, 'put').mockReturnValue(throwError(thrownError));

    domainApiService.put('path').subscribe({
      error: err => {
        expect(err.message).toBe('Error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
    });
  });

  it('Delete Method call', () => {
    jest
      .spyOn(httpService, 'delete')
      .mockReturnValue(Observable.create(getMockResponse));

    domainApiService.delete('path').subscribe({
      next: (val: any) => {
        expect(val).toEqual(getMockResponse);
        expect(httpService.delete).toBeCalledTimes(1);
      },
    });
  });

  it('Delete Method call throw error with status 500 rxJSRetryService should throw this error', done => {
    jest.spyOn(rxJSRetryService, 'genericRetryStrategy');
    jest.spyOn(httpService, 'delete').mockReturnValue(throwError(thrownError));

    domainApiService.delete('path').subscribe({
      error: err => {
        expect(err.message).toBe('Error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
    });
  });
});
