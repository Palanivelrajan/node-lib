import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { RxJSRetryService } from './rxjs-retry.service';
import { of, Observable } from 'rxjs';
import { retryWhen } from 'rxjs/operators';
import { Observer } from 'zen-observable-ts';
import { AppLoggerService } from '../../logger';
const retryMax = 3;
const createSource = (retryMax = 2): Observable<any> => {
  let retryCount = 0;
  return Observable.create((observer: Observer<any>) => {
    if (retryCount < retryMax) {
      retryCount++;
      const thrownError = new Error('Error' + retryCount);
      (thrownError as any).status = 400;
      observer.error(thrownError);
    } else {
      observer.next(retryMax);
      observer.complete();
    }
  });
};

describe('RxjsService', () => {
  let rxJSRetryService: RxJSRetryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RxJSRetryService, AppLoggerService],
    }).compile();

    rxJSRetryService = module.get<RxJSRetryService>(RxJSRetryService);
    //jest.setTimeout(30000);
  });

  it('should be defined', () => {
    expect(rxJSRetryService).toBeDefined();
  });
  describe('genericRetryStrategy', () => {
    it('should retry and pass', () => {
      let finalVal = 0;
      const source = createSource(retryMax - 1);
      source
        .pipe(
          retryWhen(
            rxJSRetryService.genericRetryStrategy({
              numberOfAttempts: retryMax,
              delayTime: 10,
              ignoredErrorCodes: [],
            }),
          ),
        )
        .subscribe({
          next: (val: any) => {
            finalVal = val;
            expect(finalVal).toBe(retryMax - 1);
          },
        });
    });
    it('should retry and fail after max attempts', done => {
      const source = createSource(retryMax + 1);
      source
        .pipe(
          retryWhen(
            rxJSRetryService.genericRetryStrategy({
              numberOfAttempts: retryMax - 1,
              delayTime: 10,
              ignoredErrorCodes: [],
            }),
          ),
        )
        .subscribe({
          error: err => {
            expect(err.message).toBe('Error' + retryMax);
            done();
          },
        });
    });
    it('should retry and fail from ignoredError option', done => {
      const source = createSource(retryMax);
      source
        .pipe(
          retryWhen(
            rxJSRetryService.genericRetryStrategy({
              numberOfAttempts: retryMax,
              delayTime: 10,
              ignoredErrorCodes: [400],
            }),
          ),
        )
        .subscribe({
          error: err => {
            expect(err.message).toBe('Error' + 1);
            done();
          },
        });
    });
  });
});
