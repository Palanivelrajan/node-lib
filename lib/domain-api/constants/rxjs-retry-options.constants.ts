import { RxJSRetryOptions } from '../interfaces/rxjs-retry-options.interface';
export const defaultRetryOptions: RxJSRetryOptions = {
  numberOfAttempts: 3,
  delayTime: 100,
  ignoredErrorCodes: [],
};
