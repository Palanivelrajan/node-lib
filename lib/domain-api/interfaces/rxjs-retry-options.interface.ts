export interface RxJSRetryOptions {
  numberOfAttempts: number;
  delayTime: number;
  ignoredErrorCodes: any[];
}
