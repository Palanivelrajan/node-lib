export interface LogMessage {
  Title: string;
  Type: string;
  Detail: string;
  Extensions?: JSON;
  Instance?: string;
  Status: string;
}
