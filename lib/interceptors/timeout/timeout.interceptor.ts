import {
  CallHandler,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
  RequestTimeoutException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable, throwError } from 'rxjs';
import { timeoutWith } from 'rxjs/operators';
import { ConfigService } from '../../config';
@Injectable()
export class TimeoutInterceptor implements NestInterceptor {
  constructor(
    private readonly config: ConfigService,
    private readonly reflector: Reflector,
  ) {}
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const requestTimeoutCategory = this.reflector.get<string>(
      'request-timeout',
      context.getHandler(),
    );
    let requestTimeout: number;
    const requestTimeoutCategoryFiltered = this.config.getRequestCategoryTimeout(
      requestTimeoutCategory,
    );
    if (requestTimeoutCategoryFiltered) {
      requestTimeout = requestTimeoutCategoryFiltered.timeout;
    } else {
      requestTimeout = 5000;
    }
    return next
      .handle()
      .pipe(
        timeoutWith(requestTimeout, throwError(new RequestTimeoutException())),
      );
  }
}
