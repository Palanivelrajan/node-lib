import { Test, TestingModule } from '@nestjs/testing';
import { INTERCEPTOR_MODULE_OPTIONS } from '../constants/interceptor.constants';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { TimeoutInterceptor } from './timeout.interceptor';
import { Reflector } from '@nestjs/core';
import { of } from 'rxjs';
import { ConfigModule } from '../../config';

const mockLogger = { verbose: jest.fn() };
const returnResponse = { name: 'This is testing' };
const mockCallHandler: any = {
  handle: () => of(returnResponse),
};

const mockContext: any = {
  getHandler: () => {
    return 'fetchOracleQueryData';
  },
  switchToHttp: () => ({
    getRequest: () => ({
      raw: () => {
        method: () => jest.fn().mockReturnValue('POST');
        url: 'mock-url';
      },
    }),
    getResponse: () => {
      const response = {
        code: code => {
          response.code = code;
          return response;
        },
        send: data => {
          return data;
        },
      };
      return response;
    },
  }),
};

describe('TimeoutInterceptor', () => {
  let timeoutInterceptor: TimeoutInterceptor;
  let reflector: Reflector;
  beforeEach(async () => {
    const loggerOptions = { appPath: process.cwd() };
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/lib/config' })],
      providers: [
        TimeoutInterceptor,
        AppLoggerService,
        Reflector,
      ],
    }).compile();
    timeoutInterceptor = module.get<TimeoutInterceptor>(TimeoutInterceptor);
    reflector = module.get<Reflector>(Reflector);
  });

  it('should be defined', () => {
    expect(timeoutInterceptor).toBeDefined();
  });

  it('should successfully return', done => {
    // if your interceptor has logic that depends on the context
    // you can always pass in a mock value instead of an empty object
    // just make sure to mock the expected alls like switchToHttp
    // and getRequest

    jest.spyOn(reflector, 'get').mockReturnValue('query');
    timeoutInterceptor.intercept(mockContext, mockCallHandler).subscribe({
      next: value => {
        expect(value).toBeTruthy();
      },
      error: error => {
        throw error;
      },
      complete: () => {
        done();
      },
    });
  });

  it('should successfully return', done => {
    // if your interceptor has logic that depends on the context
    // you can always pass in a mock value instead of an empty object
    // just make sure to mock the expected alls like switchToHttp
    // and getRequest

    jest.spyOn(reflector, 'get').mockReturnValue('query1');
    timeoutInterceptor.intercept(mockContext, mockCallHandler).subscribe({
      next: value => {
        expect(value).toBeTruthy();
      },
      error: error => {
        throw error;
      },
      complete: () => {
        done();
      },
    });
  });
});
