import { Global, Module } from '@nestjs/common';
import { LoggerModule } from '../logger/logger.module';
import { LoggingInterceptor } from './logger/logging.interceptor';
import { TimeoutInterceptor } from './timeout/timeout.interceptor';
import { TransformInterceptor } from './transform/transform.interceptor';

@Global()
@Module({
  imports: [LoggerModule],
  providers: [TransformInterceptor, LoggingInterceptor, TimeoutInterceptor],
})
export class InterceptorsModule {}
