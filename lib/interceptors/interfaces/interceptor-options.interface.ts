import { RequestCategoryTimeout } from './request-category-timeout.interface';
export class InterceptorOptions {
  requestTimeout: RequestCategoryTimeout[];
}
