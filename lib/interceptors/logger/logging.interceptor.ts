import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppLoggerService } from './../../logger/services/app.logger.service';
@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(private logger: AppLoggerService) {}
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const now = Date.now();
    const request = ctx.getRequest().body || {};
    const method = ctx.getRequest().raw.method;
    const url = ctx.getRequest().raw.url;
    return next.handle().pipe(
      tap(response => {
        const logdet = {
          requestdatetime: now,
          duration: Date.now() - now,
          response,
          request,
          method,
          url,
        };

        const message = {
          Title: 'Logger.interceptor',
          Type: 'Log',
          Detail: JSON.stringify(logdet),
          Status: 'Status',
          Extension: '',
        };

        this.logger.verbose(message);
      }),
    );
  }
}
