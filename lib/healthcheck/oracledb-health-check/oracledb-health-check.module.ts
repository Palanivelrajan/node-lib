import { Module } from '@nestjs/common';
import { OracledbHealthCheckService } from './service/oracledb-health-check.service';

@Module({
  providers: [OracledbHealthCheckService],
  exports: [OracledbHealthCheckService],
})
export class OracledbHealthCheckModule {}
