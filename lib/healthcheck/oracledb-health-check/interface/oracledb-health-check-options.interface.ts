export interface OracleDBHealthCheckOptions {
  name: string;
  user: string;
  password: string;
  connectString: string;
}
