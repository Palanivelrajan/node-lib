import { Test, TestingModule } from '@nestjs/testing';
import { OracledbHealthCheckService } from './oracledb-health-check.service';
import { DependencyUtlilizationService } from '../../dependency-utlilization/dependency-utlilization.service';
import { OracleDBService } from './../../../database/oracledb/service/oracledb.service';
import { ORACLE_DB_OPTIONS } from '../../../database/oracledb';
import { throwError, Observable, of } from 'rxjs';
import { async } from 'rxjs/internal/scheduler/async';
import { HealthStatus } from '../../enums/health-status.enum';
import { ConfigModule } from '../../../config';
import { AppLoggerService } from '../../../logger';

describe('OracledbHealthCheckService', () => {
  let oracledbHealthCheckService: OracledbHealthCheckService;
  let oracleDBService: OracleDBService;
  let appLoggerService: AppLoggerService;
  let dependencyUtlilizationService: DependencyUtlilizationService;
  let oracleDBOptions = [
    {
      name: 'rfdest',
      user: 'rfdest',
      password: 'rfdest',
      connectString: 'alsyntstb.ohlogistics.com:1521/syntstb',
    },
    {
      name: 'DOCGENDEV',
      user: 'DOCGENDEV',
      password: 'DocGen$dev18',
      connectString: 'alsynwebd.ohlogistics.com/synwebd_maint',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TestingModule, ConfigModule.forRoot({ folder: '/lib/config' })],
      providers: [
        OracledbHealthCheckService,
        DependencyUtlilizationService,
        OracleDBService,
        AppLoggerService,
      ],
    }).compile();

    oracledbHealthCheckService = module.get<OracledbHealthCheckService>(
      OracledbHealthCheckService,
    );

    oracleDBService = module.get<OracleDBService>(OracleDBService);
    dependencyUtlilizationService = module.get<DependencyUtlilizationService>(
      DependencyUtlilizationService,
    );
    appLoggerService = module.get<AppLoggerService>(AppLoggerService);
  });

  it('should be defined', () => {
    expect(oracledbHealthCheckService).toBeDefined();
  });

  it('isHealthy should return status as true when pingCheck return true', () => {
    jest
      .spyOn(dependencyUtlilizationService, 'isRecentlyUsed')
      .mockReturnValue(true);

    const result = oracledbHealthCheckService.isHealthy(oracleDBOptions);

    result.then(response =>
      expect(response['rfdest'].status).toBe(HealthStatus.pass),
    );
  });
  it('isHealthy should return status as false when pingCheck return false', () => {
    jest
      .spyOn(dependencyUtlilizationService, 'isRecentlyUsed')
      .mockReturnValue(false);

    jest
      .spyOn(oracleDBService, 'executeQuery')
      .mockImplementation(async (dbname, query, bindValue) => {
        throw new Error('Connection string does not exist');
      });

    jest.spyOn(appLoggerService, 'error').mockImplementation(() => {});
    const result = oracledbHealthCheckService.isHealthy(oracleDBOptions);

    result
      .then(response => {
        expect(response['rfdest'].status).toBe(HealthStatus.fail);
        //console.log('response' + response['rfdest']);
      })
      .catch(reject => {
        expect(reject['rfdest'].status).toBe(HealthStatus.fail);
        //console.log('reject' + reject['rfdest']);
      });
  });
});
