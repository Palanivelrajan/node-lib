import { HttpModule, Module } from '@nestjs/common';
import { ApiHealthIndicator } from './service/api-health-indicator';

@Module({
  imports: [HttpModule],
  providers: [ApiHealthIndicator],
  exports: [ApiHealthIndicator],
})
export class ApiHealthCheckModule {}
