import { Module } from '@nestjs/common';
import { PostgressdbHealthCheckService } from './service/postgressDB-health-check.service';

@Module({
  providers: [PostgressdbHealthCheckService],
  exports: [PostgressdbHealthCheckService],
})
export class PostgressdbHealthCheckModule {}
