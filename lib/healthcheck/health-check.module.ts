import { Module } from '@nestjs/common';
import { AppLoggerService } from '../logger';
import { ApiHealthCheckModule } from './api-health-check/api-health-check.module';
import { DependencyUtlilizationService } from './dependency-utlilization/dependency-utlilization.service';
import { HealthCheckController } from './health-check.controller';
import { HealthCheckService } from './health-check.service';
import { OracledbHealthCheckModule } from './oracledb-health-check/oracledb-health-check.module';
import { PostgressdbHealthCheckModule } from './postgressdb-health-check/postgressdb-health-check.module';
@Module({
  imports: [
    ApiHealthCheckModule,
    PostgressdbHealthCheckModule,
    OracledbHealthCheckModule,
    AppLoggerService,
  ],
  providers: [HealthCheckService, DependencyUtlilizationService],
  controllers: [HealthCheckController],
  exports: [DependencyUtlilizationService],
})
export class HealthCheckModule {}
