import { Injectable } from '@nestjs/common';
@Injectable()
export class DependencyUtlilizationService {
  private dependencyUtlilization: any;
  private healthCheckDuration: number;
  constructor() {
    this.dependencyUtlilization = {};
    this.healthCheckDuration = 5000;
  }
  currentTimeStamp(): number {
    return Date.now();
  }
  getLastUsedTimeStamp(key: string): number {
    return this.dependencyUtlilization[key];
  }

  updateTimeStamp(key: string): void {
    this.dependencyUtlilization[key] = this.currentTimeStamp();
  }

  isRecentlyUsed(key: string): boolean {
    if (this.dependencyUtlilization[key]) {
      return (
        Math.abs(this.currentTimeStamp() - this.dependencyUtlilization[key]) <=
        this.healthCheckDuration
      );
    }
    return false;
  }
}
