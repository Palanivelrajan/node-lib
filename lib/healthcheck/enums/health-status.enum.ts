export enum HealthStatus {
  pass = 'pass',
  fail = 'fail',
  warn = 'warn',
}
