import { Injectable } from '@nestjs/common';
import { HealthCheckResult } from '../healthcheck/interfaces/health-check-result.interface';
import { ApiHealthIndicator } from './api-health-check/service/api-health-indicator';
import { HealthStatus } from './enums/health-status.enum';
import { PostgressdbHealthCheckService } from './postgressdb-health-check/service/postgressDB-health-check.service';
import { OracledbHealthCheckService } from './oracledb-health-check/service/oracledb-health-check.service';
import { ConfigService } from '../config';
import { async } from 'rxjs/internal/scheduler/async';
import { ApiHealthCheckModule } from './api-health-check';
import { ComponentHealthCheckResult } from './interfaces';
import * as _async from 'neo-async';
@Injectable()
export class HealthCheckService {
  private healthCheckResult: HealthCheckResult;
  private isHealthyStatus = HealthStatus.pass;
  constructor(
    private readonly config: ConfigService,
    private readonly apiHealthIndicator: ApiHealthIndicator,
    private readonly postgressdbHealthCheckService: PostgressdbHealthCheckService,
    private readonly oracledbHealthCheckService: OracledbHealthCheckService,
  ) {}

  async isHealthy(): Promise<HealthCheckResult> {
    this.healthCheckResult = {
      status: this.isHealthyStatus,
      version: 'v1.0.0',
      releaseID: 'release1',
      notes: [' '],
      output: ' ',
      serviceID: '123123',
      description: `Health status of the BFF application is ${this.isHealthyStatus}.`,
      details: [],
      links: {},
    };

    return Promise.all([
      this.postgressDBHealthCheck(),
      this.oracleDBHealthCheck(),
      this.ApiHealthCheck(),
    ])
      .then(details => {
        this.healthCheckResult.details = details;
        let exitFor: boolean = false;
        for (const entry of details) {
          const arrayresult = Object.keys(entry);
          for (let i = 0; i < arrayresult.length; i++) {
            if (entry[arrayresult[i]].status === HealthStatus.fail) {
              this.isHealthyStatus = HealthStatus.fail;
              exitFor = true;
              break;
            } else if (entry[arrayresult[i]].status === HealthStatus.warn) {
              this.isHealthyStatus = HealthStatus.warn;
            }
          }
          if (exitFor) {
            break;
          }
        }
        this.healthCheckResult.status = this.isHealthyStatus;
        return this.healthCheckResult;
      })
      .catch(reject => {
        this.healthCheckResult.details = reject;
        return this.healthCheckResult;
      });
  }

  async postgressDBHealthCheck(): Promise<{
    [key: string]: ComponentHealthCheckResult;
  }> {
    const postgressDBHealthCheckResult = await this.postgressdbHealthCheckService.isHealthy(
      this.config.databaseOptions.postgressDB,
    );

    return postgressDBHealthCheckResult;
  }

  async oracleDBHealthCheck(): Promise<{
    [key: string]: ComponentHealthCheckResult;
  }> {
    const oracleDBHealthCheckResult = await this.oracledbHealthCheckService.isHealthy(
      this.config.databaseOptions.oracleDB,
    );

    return oracleDBHealthCheckResult;
  }

  async ApiHealthCheck(): Promise<{
    [key: string]: ComponentHealthCheckResult;
  }> {
    const apiHealthCheckResult = await this.apiHealthIndicator.isHealthy(
      this.config.domainAPIOptions,
    );
    return apiHealthCheckResult;
  }
}
