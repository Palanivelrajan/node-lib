import { PostgressDBOptions } from '../postgress-db/interface/postgress-db-options.interface';
import { OracleDBOptions } from '../oracledb';

export interface DatabaseOptions {
  postgressDB: PostgressDBOptions[];
  oracleDB: OracleDBOptions[];

}
