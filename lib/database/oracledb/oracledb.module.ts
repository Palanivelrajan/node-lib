import { Module, DynamicModule, Global } from '@nestjs/common';
import { OracleDBService } from './service/oracledb.service';
import { ConfigModule, ConfigOptions } from '../../config';

@Global()
@Module({
  providers: [OracleDBService],
  exports: [OracleDBService],
})
export class OracleDBModule {}
