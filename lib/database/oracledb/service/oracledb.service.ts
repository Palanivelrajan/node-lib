import { Injectable } from '@nestjs/common';
import oracledb from 'oracledb';
import { PromInstanceCounter, PromMethodCounter } from '@digikare/nestjs-prom';
import { ConfigService } from '../../../config';
import { timer } from 'rxjs';
import { AppLoggerService, LogMessage } from '../../../logger';

@PromInstanceCounter
@Injectable()
export class OracleDBService {
  constructor(
    private readonly config: ConfigService,
    private appLoggerService: AppLoggerService,
  ) {}
  @PromMethodCounter()
  async executeQuery(
    dbName: string,
    query: string,
    bindValue: any[] = [],
  ): Promise<any> {
    try {
      const conn = await this.getOracleConnection(dbName);
      return conn.execute(query, bindValue);
    } catch (err) {
      throw err;
    }
  }

  @PromMethodCounter()
  async getOracleConnection(dbName: string) {
    const oracleDbOption = this.config.getOracelDatabaseOption(dbName);
    if (!oracleDbOption) {
      throw new Error('Connection string does not exist');
    } else {
      return await oracledb.getConnection({
        user: oracleDbOption.user,
        password: oracleDbOption.password,
        connectString: oracleDbOption.connectString,
      });
    }
  }

  async executeStoredProcReturnCursor(
    dbname: string,
    storedproc: string,
    parameter: any,
  ) {
    let result: any;
    const numRows = 100;
    const dbResult = [];
    let rows;
    let arrayresult;

    arrayresult = Object.keys(parameter);

    const plsqlstatement =
      `BEGIN ` + storedproc + `(: ` + arrayresult.join(', :') + ` ); END; `;

    try {
      const localConnection = this.getOracleConnection('rfdest');
      await localConnection.then(
        async connection =>
          (result = await connection.execute(plsqlstatement, parameter)),
      );

      do {
        rows = await result.outBinds.out_tasks.getRows(numRows); // get numRows rows at a time
        dbResult.push(...rows);
      } while (rows.length === numRows);

      return dbResult;
    } catch (error) {
      const message: LogMessage = {
        Title: 'This is OracleDB Stored Proc Execution',
        Type: 'OracleDB Service',
        Detail: error,
        Status: 'Open',
      };
      this.appLoggerService.log(message);
    }
  }
}
