import { Test, TestingModule } from '@nestjs/testing';
import { OracleDBService } from './oracledb.service';
import { ORACLE_DB_OPTIONS } from '../constant';
import { OracleDBOptions } from '../interface';
import oracledb from 'oracledb';
import { ConfigModule, ConfigService } from '../../../config';
import { AppLoggerService } from '../../../logger';
import { throws } from 'assert';

describe('OracleDbService', () => {
  let oracleDBService: OracleDBService;
  let configService: ConfigService;
  let appLoggerService: AppLoggerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/lib/config' })],
      providers: [OracleDBService, AppLoggerService],
    }).compile();

    oracleDBService = module.get<OracleDBService>(OracleDBService);
    appLoggerService = module.get<AppLoggerService>(AppLoggerService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(oracleDBService).toBeDefined();
  });

  it('OracleDBService executeQuery success', () => {
    jest.mock('oracledb');
    const getConnMockValue = {
      execute: jest.fn().mockReturnThis(),
      close: jest.fn().mockReturnThis(),
    };
    jest
      .spyOn(oracleDBService, 'getOracleConnection')
      .mockResolvedValue(getConnMockValue);

    oracleDBService.executeQuery('rfdest', '').then(x => {
      expect(getConnMockValue.execute).toHaveBeenCalled();
    });
  });

  it('OracleDBService getConnection throw error', () => {
    const getConnMockValue = {
      execute: jest.fn(() => {
        throw new Error('Connection string does not exist');
      }),
      close: jest.fn().mockReturnThis(),
    };
    jest
      .spyOn(oracleDBService, 'getOracleConnection')
      .mockResolvedValue(getConnMockValue);

    oracleDBService.executeQuery('rfdest', '').catch(x => {
      expect(x).toStrictEqual(new Error('Connection string does not exist'));
    });
  });

  it('get Oracle Connection throw error - connection string does not exist ', () => {
    jest.mock('oracledb');
    jest.spyOn(configService, 'getOracelDatabaseOption').mockReturnValue(null);
    oracleDBService.getOracleConnection('rfdest').then(x => {
      expect(x).toBe('Connection string does not exist');
    });
  });

  it('get Oracle Connection - return connection string', () => {
    const obj: OracleDBOptions = {
      name: '',
      user: '',
      password: '',
      connectString: '',
    };

    jest.mock('oracledb');
    jest.spyOn(configService, 'getOracelDatabaseOption').mockReturnValue(obj);
    oracleDBService.getOracleConnection('rfdest').then(x => {
      expect(oracledb.getConnection).toHaveBeenCalled();
    });
  });
});
