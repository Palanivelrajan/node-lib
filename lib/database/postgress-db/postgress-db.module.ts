import { Module, Global, DynamicModule } from '@nestjs/common';
import { PostgressDBService } from './service';
import { ConfigModule, ConfigOptions } from '../../config';
@Global()
@Module({
  providers: [PostgressDBService],
  exports: [PostgressDBService],
})
export class PostgressDBModule {}
