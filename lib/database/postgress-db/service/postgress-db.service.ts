import { Injectable } from '@nestjs/common';
import { Pool } from 'pg';
import { ConfigService } from '../../../config';
@Injectable()
export class PostgressDBService {
  constructor(private readonly config: ConfigService) {}

  async executeQuery(dbName: string, query: string, bindValue: any[] = []) {
    try {
      const postgressDBOption = this.config.getPostgresDatabaseOption(dbName);
      if (!postgressDBOption) {
        throw Error('Connection string does not exist');
      } else {
        const pool = new Pool({
          user: postgressDBOption.user,
          host: postgressDBOption.host,
          database: postgressDBOption.database,
          password: postgressDBOption.password,
          port: postgressDBOption.port,
        });
        return await pool.query(query, bindValue);
      }
    } finally {
      // if (localConnection) {
      //   try {
      //     await localConnection.close();
      //   } catch (err) {
      //     //console.log('Error when closing the database connection: ', err);
      //   }
      // }
    }
  }
}
