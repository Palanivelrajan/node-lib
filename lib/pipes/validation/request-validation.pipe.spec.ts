import { RequestValidationPipe } from './request-validation.pipe';

describe('RequestValidationPipe', () => {
  it('should be defined', () => {
    expect(new RequestValidationPipe()).toBeDefined();
  });

  it('should return the path', () => {
    const pipe = new RequestValidationPipe();
    const path = pipe.transform('value', null);
    expect(path).toEqual('value');
  });
});
