import { Module } from '@nestjs/common';
import { EmitDirectService, EmitTopicService, NewTaskService } from './service';
import { EmitService } from './service/emit.service';
@Module({
  providers: [EmitService, EmitDirectService, EmitTopicService, NewTaskService],
  exports: [EmitService, EmitDirectService, EmitTopicService, NewTaskService],
})
export class RabbitmqModule {}
