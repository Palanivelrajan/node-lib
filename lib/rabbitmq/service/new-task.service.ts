import { Injectable } from '@nestjs/common';
import * as amqp from 'amqplib';
import { ConfigService } from '../../config';

@Injectable()
export class NewTaskService {
  constructor(
    private readonly config: ConfigService,
  ) { }
  publish(queueName: string, message: string) {
    const rabbitMQOption = this.config.getRabbitmqOption(queueName);
    if (!rabbitMQOption) {
      throw Error('Connection string does not exist');
    }

    amqp
      .connect(rabbitMQOption.connection)
      .then((conn) => {
        return conn
          .createChannel()
          .then((ch) => {
            const q = rabbitMQOption.name;
            const ok = ch.assertQueue(q, { durable: true });

            return ok.then(() => {
              const msg = message;
              ch.sendToQueue(q, Buffer.from(msg), { deliveryMode: true });
              return ch.close();
            });
          })
          .finally(() => {
            conn.close();
          });
      })
      .catch(console.warn);
  }
}
