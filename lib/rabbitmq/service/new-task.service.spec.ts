import { Test, TestingModule } from '@nestjs/testing';
import { NewTaskService } from './new-task.service';
import { RABBITMQ_OPTIONS } from '../constant';
import { ConfigModule } from '../../config';

describe('NewTaskService', () => {
  let newTaskService: NewTaskService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/lib/config' })],
      providers: [NewTaskService],
    }).compile();

    newTaskService = module.get<NewTaskService>(NewTaskService);
  });

  it('should be defined', () => {
    expect(newTaskService).toBeDefined();
  });

  // it('call publish with queue Name and check it is called', () => {
  //   jest.spyOn(newTaskService, 'publish');
  //   newTaskService.publish('synapsebffcore', 'This is unit testing');
  //   expect(newTaskService.publish).toBeCalled();
  // });
});
