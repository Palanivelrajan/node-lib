import { Test, TestingModule } from '@nestjs/testing';
import { EmitService } from './emit.service';
import { RABBITMQ_OPTIONS } from '../constant';
import { RabbitMQOptions } from '../interface';
import { ConfigModule } from '../../config';
import * as amqp from 'amqplib';
describe('EmitLogService', () => {
  let emitService: EmitService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/lib/config' })],
      providers: [EmitService],
    }).compile();

    emitService = module.get<EmitService>(EmitService);
  });

  it('should be defined', () => {
    expect(emitService).toBeDefined();
  });

  // it('call publish with invalid queueName and check exception throw.', () => {
  //   try {
  //     emitService.publish('', 'This is unit testing');
  //   } catch (ex) {
  //     expect(ex).not.toBeNull();
  //   }
  // });

  // it('call publish with queue Name and check it is called', () => {
  //   jest.spyOn(emitService, 'publish');
  //   emitService.publish('synapsebffcore', 'This is unit testing');
  //   expect(emitService.publish).toBeCalled();
  // });
});
