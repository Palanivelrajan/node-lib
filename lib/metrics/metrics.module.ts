import { PromModule } from '@digikare/nestjs-prom';
import { Module } from '@nestjs/common';
import { MetricsController } from './controller/metrics.controller';
@Module({
  imports: [
    PromModule.forRoot({
      withDefaultController: false,
    }),
  ],
  controllers: [MetricsController],
})
export class MetricsModule {}
