import { DynamicModule, Global, Module } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { OracleDBModule } from './database/oracledb';
import { PostgressDBModule } from './database/postgress-db';
import { DomainApiModule } from './domain-api/';
import { FiltersModule } from './filters/filters.module';
import { HealthCheckModule } from './healthcheck/health-check.module';
import { InterceptorsModule } from './interceptors/interceptors.module';
import { LoggerModule } from './logger';
import { MetricsModule } from './metrics/metrics.module';
import { RabbitmqModule } from './rabbitmq/rabbitmq.module';
import { SynapseBFFCoreOptions } from './synapsebffcore-options.interface';

@Global()
@Module({})
export class SynapseBffCoreModule {
  static forRoot(options?: SynapseBFFCoreOptions): DynamicModule {
    return {
      module: SynapseBffCoreModule,
      imports: [
        ConfigModule.forRoot(options.configOptions),
        LoggerModule,
        InterceptorsModule,
        FiltersModule,
        DomainApiModule,
        HealthCheckModule,
        MetricsModule,
        OracleDBModule,
        PostgressDBModule,
        RabbitmqModule,
      ],
      exports: [
        ConfigModule,
        LoggerModule,
        InterceptorsModule,
        FiltersModule,
        DomainApiModule,
        HealthCheckModule,
        OracleDBModule,
        MetricsModule,
        PostgressDBModule,
        RabbitmqModule,
      ],
    };
  }
}
