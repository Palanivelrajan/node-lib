import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { AppLoggerService } from '../../logger';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private logger: AppLoggerService) { }

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const message = {
      Title: exception.name,
      Type: 'Error',
      Detail: exception.message,
      Status: 'Status',
      Extension: '',
    };
    this.logger.error(message, '');

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    response.status(status).send({
      statusCode: status,
      message,
      timestamp: 'Exception - AllExceptionsFilter ' + new Date().toISOString(),
      path: request.url,
    });
  }
}
