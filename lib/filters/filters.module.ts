import { Global, Module } from '@nestjs/common';
import { LoggerModule } from '../logger';
import { AllExceptionsFilter } from './all-exceptions/all-exceptions.filter';
import { BadRequestExceptionFilter } from './bad-request-exception/badrequest-exception.filter';
import { HttpExceptionFilter } from './http-exception/http-exception.filter';
import { RequestTimeoutExceptionFilter } from './request-timeout-exception/request-timeout-exception.filter';

@Global()
@Module({
  imports: [LoggerModule],
  providers: [
    AllExceptionsFilter,
    BadRequestExceptionFilter,
    HttpExceptionFilter,
    RequestTimeoutExceptionFilter,
  ],
})
export class FiltersModule {}
