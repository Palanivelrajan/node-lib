import { RequestTimeoutExceptionFilter } from './request-timeout-exception.filter';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { of } from 'rxjs';
import { RequestTimeoutException } from '@nestjs/common';

const mockLogger = { error: jest.fn() }




const mockContext: any = {
  switchToHttp: () => ({
    getRequest: () => ({
      url: 'mock-url',
    }),
    getResponse: () => {
      const response = {
        code: code => {
          response.code = code;
          return response;
        },
        send: data => {
          return data;
        },
      };
      return response;
    },
  }),
};

describe('RequestTimeoutExceptionFilter', () => {
  let filter: RequestTimeoutExceptionFilter;

  beforeEach(() => {
    filter = new RequestTimeoutExceptionFilter(mockLogger as any);
  });

  it('should catch and log the error', () => {
    const mockException: RequestTimeoutException = new RequestTimeoutException();

    mockException.name = 'RequestTimeoutExceptionFilter';
    mockException.getResponse = jest.fn().mockReturnValue(of('getResponse'));
    mockException.getStatus = () => 404;

    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });
  it('should catch and log the error', () => {
    const mockExceptionTypeAny: any = {
      name: 'RequestTimeoutExceptionFilter',
      message: 'This is message details',
      getResponse: jest.fn().mockReturnValue(of('getResponse')),
      getStatus: () => 404,
    };
    filter.catch(mockExceptionTypeAny, mockContext);
    expect(mockLogger.error).toBeCalled();
  });

});