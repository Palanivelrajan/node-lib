import { HttpExceptionFilter } from './http-exception.filter';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { TestingModule, Test } from '@nestjs/testing';
import { of } from 'rxjs';

const mockLogger = { error: jest.fn() }



const mockContext: any = {
  switchToHttp: () => ({
    getRequest: () => ({
      url: "mock-url"
    }),
    getResponse: () => {
      const response = {
        code: code => {
          response.code = code;
          return response;
        },
        send: data => {
          return data;
        },
      };
      return response;
    },
  }),
};

describe('HttpExceptionFilter', () => {
  let filter: HttpExceptionFilter;

  beforeEach(() => {
    filter = new HttpExceptionFilter(mockLogger as any);
  });

  it('should catch and log the error', () => {
    const mockException: any = {
      name: 'HttpExceptionFilter',
      message: 'This is message details',
      getResponse: jest.fn().mockReturnValue(of('getResponse')),
      getStatus: () => 404,
    };

    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });

  it('should catch and log the error', () => {
    const mockException: any = {
      name: 'HttpExceptionFilter',
      message: 'This is message details',
      getResponse: jest.fn().mockReturnValue(of('getResponse')),
    };
    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });

});
