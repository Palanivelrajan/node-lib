import { BadRequestExceptionFilter } from './badrequest-exception.filter';
import { TestingModule, Test } from '@nestjs/testing';
import { of } from 'rxjs';
import { BadRequestException } from '@nestjs/common';

const mockLogger = { error: jest.fn() };
const mockContext: any = {
  switchToHttp: () => ({
    getRequest: () => ({
      url: 'mock-url',
    }),
    getResponse: () => {
      const response = {
        code: jest.fn().mockReturnThis(),
        send: jest.fn().mockReturnThis(),
      };
      return response;
    },
  }),
};
describe('BadRequestExceptionFilter', () => {
  let filter: BadRequestExceptionFilter;

  beforeEach(() => {
    filter = new BadRequestExceptionFilter(mockLogger as any);
  });

  it('should catch and log the error', () => {
    const mockException: BadRequestException = new BadRequestException();

    mockException.name = 'BadRequestException';
    mockException.getResponse = jest.fn().mockReturnValue(of('getResponse'));
    mockException.getStatus = () => 404;

    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });

  it('should catch and log the error', () => {
    const mockException: any = {
      name: 'BadRequestException',
      message: 'This is message details',
      getResponse: jest.fn().mockReturnValue(of('getResponse')),
      getStatus: () => 404,
    };

    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });
});
