import { ReflectMetadata, SetMetadata } from '@nestjs/common';

// export const RequestTimeout = (args: string) => ReflectMetadata('request-timeout', args);
export const RequestTimeout = (args: string) =>
  SetMetadata('request-timeout', args);
