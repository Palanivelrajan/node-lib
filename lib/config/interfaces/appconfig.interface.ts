import { DomainAPIOptions } from '../../domain-api/interfaces/domain-api-options.interface';
import { FilterOptions } from '../../filters/interfaces/filter-options.interface';
import { HealthCheckOptions } from '../../healthcheck/interfaces/health-check-options.interface';
import { InterceptorOptions } from '../../interceptors/interfaces/interceptor-options.interface';
import { LoggerOptions } from '../../logger/interfaces/logger-options.interface';
import { RabbitMQOptions } from '../../rabbitmq/interface/rabbitmq-options.interface';
import { DatabaseOptions } from '../../database/interface';
export interface AppConfig {
  filter: FilterOptions;
  interceptors: InterceptorOptions;
  logger: LoggerOptions;
  domainApi: DomainAPIOptions[];
  healthCheck: HealthCheckOptions;
  database: DatabaseOptions;
  rabbitmq: RabbitMQOptions[];
}
