import { Inject, Injectable, Global, DynamicModule } from '@nestjs/common';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import * as path from 'path';
import { OracleDBOptions } from '../database/oracledb/interface/oracledb-options.interface';
import { PostgressDBOptions } from '../database/postgress-db/interface/postgress-db-options.interface';
import { DomainAPIOptions } from '../domain-api/interfaces/domain-api-options.interface';
import { LoggerOptions } from '../logger/interfaces/logger-options.interface';
import { RabbitMQOptions } from '../rabbitmq/interface/rabbitmq-options.interface';
import { CONFIG_OPTIONS } from './constants';
import { AppConfig, ConfigOptions } from './interfaces';
import { HealthCheckOptions } from '../healthcheck/interfaces/health-check-options.interface';
import { InterceptorOptions } from '../interceptors';
import { RequestCategoryTimeout } from '../interceptors/interfaces/request-category-timeout.interface';
import { DatabaseOptions } from '../database/interface';

@Global()
@Injectable()
export class ConfigService {
  private appConfig: AppConfig;

  constructor(@Inject(CONFIG_OPTIONS) options: ConfigOptions) {
    this.loadYamlConfig(options);
  }

  private loadYamlConfig(options?: ConfigOptions) {
    const configFolder = options ? options.folder : '/src/config';
    const filePath = `${process.env.NODE_ENV || 'development'}.yml`;
    const envFile = path.join(process.cwd(), configFolder, filePath);
    this.appConfig = yaml.safeLoad(fs.readFileSync(envFile, 'utf8'));
  }

  get(key: string): any {
    return this.appConfig[key];
  }

  getdomainAPIOption(id: string): DomainAPIOptions {
    return this.appConfig.domainApi.find(item => item.key === id);
  }
  get domainAPIOptions(): DomainAPIOptions[] {
    return this.appConfig.domainApi;
  }

  // get loggerOptions(): LoggerOptions {
  //   return this.appConfig.logger;
  // }

  getOracelDatabaseOption(id: string): OracleDBOptions {
    return this.appConfig.database.oracleDB.find(item => item.name === id);
  }

  getPostgresDatabaseOption(id: string): PostgressDBOptions {
    return this.appConfig.database.postgressDB.find(item => item.name === id);
  }

  getRequestCategoryTimeout(id: string): RequestCategoryTimeout {
    return this.appConfig.interceptors.requestTimeout.find(
      x => x.category === id,
    );
  }

  get databaseOptions(): DatabaseOptions {
    return this.appConfig.database;
  }

  getRabbitmqOption(id: string): RabbitMQOptions {
    return this.appConfig.rabbitmq.find(item => item.name === id);
  }
}
