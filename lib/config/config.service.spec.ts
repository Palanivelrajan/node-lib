import { Test, TestingModule } from '@nestjs/testing';

import { ConfigService } from './config.service';
import { ConfigModule } from './config.module';
import { CONFIG_OPTIONS } from './constants';
describe('ConfigService', () => {
  let service: ConfigService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TestingModule],
      providers: [
        ConfigService,
        { provide: CONFIG_OPTIONS, useValue: { folder: '/lib/config' } },
      ],
    }).compile();

    service = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('config service get key', () => {
    const result = [{ key: 'localhost', url: 'http://localhost:3001' }];
    expect(service.get('domainApi')).toStrictEqual(result);
  });

  it('config service - get domain API Option', () => {
    const result = { key: 'localhost', url: 'http://localhost:3001' };
    expect(service.getdomainAPIOption('localhost')).toStrictEqual(result);
  });
});
describe('ConfigService', () => {
  let service: ConfigService;
  beforeEach(async () => {
    delete process.env.NODE_ENV;
    const module: TestingModule = await Test.createTestingModule({
      imports: [TestingModule],
      providers: [ConfigService, { provide: CONFIG_OPTIONS, useValue: false }],
    }).compile();

    service = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('config service get key', () => {
    const result = [{ key: 'localhost', url: 'http://localhost:3001' }];
    expect(service.get('domainApi')).toStrictEqual(result);
  });

  it('config service - get domain API Option', () => {
    const result = { key: 'localhost', url: 'http://localhost:3001' };
    expect(service.getdomainAPIOption('localhost')).toStrictEqual(result);
  });

  it('config service - getOracelDatabaseOption', () => {
    const result = {
      name: 'rfdest',
      user: 'rfdest',
      password: 'rfdest',
      connectString: 'alsyntstb.ohlogistics.com:1521/syntstb',
    };
    expect(service.getOracelDatabaseOption('rfdest')).toStrictEqual(result);
  });
  it('config service - getPostgresDatabaseOption', () => {
    const result = {
      name: 'shipperanalyticsq',
      user: 'usr_shipper',
      host: 'FDC00pgsp305L.ohlogistics.com',
      database: 'shipperanalyticsq',
      password: 'Sh1pP3Rq@',
      port: 5432,
    };
    expect(
      service.getPostgresDatabaseOption('shipperanalyticsq'),
    ).toStrictEqual(result);
  });
  it('config service - call getRabbitmqOption with name and check return value', () => {
    const result = {
      name: 'synapsebffcore',
      key: 'info',
      severity: 'severity',
      exchangeName: 'DirectX',
      exchangeType: 'Direct',
      connection: 'amqp://guest:guest@localhost:5672/?prefetch-count=1',
    };
    expect(service.getRabbitmqOption('synapsebffcore')).toStrictEqual(result);
  });
});
